#if defined(__MACOSX__) || (defined(__APPLE__) && defined(__MACH__))
#include <GLUT/glut.h>
#elif defined(WIN32) || defined(_WIN32) || defined(__TOS_WIN__) || defined(PREDEF_COMPILER_VISUALC)
#include <windows.h>
#include <GL/glut.h>
#elif defined(linux) || defined(__linux) || defined(__linux__)
#include <GL/glut.h>
#endif

#include <cmath>
#include <ctime>
#include <cstring>

#define TIMESTEP 100
#define RADIUS_SEC 0.4
#define RADIUS_MIN 0.6
#define RADIUS_HOUR 0.8
#define RADIUS_0_12 0.85
#define PI 3.14159265
#define rads(x) x*(PI/180)

#define ORIGIN 0.0

void init();
void display();
void keyboard(unsigned char, int, int);

void drawCircle(GLfloat, GLfloat, GLfloat);
void drawPointer(GLfloat, GLfloat, GLfloat);
void getTime();
void writeTime();
void idle(void);

GLint h, m, s;
struct tm* timeinfo;
const GLchar hours[][3] = {"1","2","3","4","5","6","7","8","9","10","11","12"};
const GLchar minsecs[][3] = {"5","10","15","20","25","30","35","40","45","50","55","0"};
GLchar timeString[9];

int main(int argc, char* argv[])
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGB);
	glutInitWindowSize(512,512);
	glutInitWindowPosition(100,100);
	glutCreateWindow("Relogio 2");

	init();

	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutIdleFunc(idle);

	glutMainLoop();
	return 1;
}

void init(void)
{
    glClearColor(0.0,0.0,0.0,1.0);
    gluOrtho2D(-1,1,-1,1);
    glShadeModel(GL_FLAT);
}

void display(void)
{
    glClear(GL_COLOR_BUFFER_BIT);

    getTime();

    glColor3f(0.75,0.75,0.75);
	drawCircle(ORIGIN,ORIGIN,RADIUS_HOUR);
	drawCircle(ORIGIN,ORIGIN,RADIUS_MIN);
	drawCircle(ORIGIN,ORIGIN,RADIUS_SEC);

	drawPointer(RADIUS_HOUR,h,8);
	drawPointer(RADIUS_MIN,m,6);
	drawPointer(RADIUS_SEC,s,4);

	writeTime();

    glutSwapBuffers();
}

void keyboard(unsigned char key, int x, int y)
{
    switch(key)
    {
    case 27:
        exit(0);
        break;
    }
}

void drawCircle(GLfloat x, GLfloat y, GLfloat radius)
{
	const GLfloat jump = PI/180;
	GLfloat angle;

	glPushMatrix();

	glTranslatef(x,y,0);

	glBegin(GL_LINE_LOOP);
		for (int i=0; i < 360; i++)
		{
		  angle = i*jump;
		  glVertex2f(cos(angle)*radius,sin(angle)*radius);
		}
	glEnd();

	glPopMatrix();
}

void drawPointer(GLfloat distance, GLfloat angle, GLfloat size)
{
	glPushMatrix();

	glTranslatef(distance*cos((PI/2)-rads(angle)),distance*sin((PI/2)-rads(angle)),0.0);
	glRotatef(-angle,0.0,0.0,1.0);
	glScalef(size,size,0);

	glBegin(GL_TRIANGLES);
		glVertex2f(0.0,0.0);
		glVertex2f(-0.01,0.02);
		glVertex2f(0.01,0.02);
	glEnd();

	glPopMatrix();
}

void writeTime()
{
	strftime(timeString,9,"%H:%M:%S",timeinfo);
	const GLfloat jump = PI/6;

	glPushMatrix();

	glRasterPos2f(-(36.0/256.0),-(7.5/256.0));
	for(int i=0; i<8; i++)
	{
		glutBitmapCharacter(GLUT_BITMAP_9_BY_15,timeString[i]);
	}

	for(int i=0; i<12; i++)
	{
		GLfloat degInRad = (i+1)*jump;
		glRasterPos2f(sin(degInRad)*RADIUS_0_12 - (strlen(hours[i])*(9.0/512.0)),cos(degInRad)*RADIUS_0_12 -(7.5/512.0));
		for(unsigned int j=0; j<strlen(hours[i]); j++)
		{
			glutBitmapCharacter(GLUT_BITMAP_9_BY_15,hours[i][j]);
		}
	}

	glPopMatrix();
}

void getTime()
{
	time_t rawtime;
	time(&rawtime);
	timeinfo = localtime(&rawtime);
	h = (360.0/12.0)*timeinfo->tm_hour + (360.0/12.0/60.0)*timeinfo->tm_min;
	m = (360.0/60.0)*timeinfo->tm_min + (360.0/60.0/60.0)*timeinfo->tm_sec;
	s = (360.0/60.0)*timeinfo->tm_sec;
}

void idle(void)
{
	glutPostRedisplay();
}
