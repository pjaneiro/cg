#if defined(__MACOSX__) || (defined(__APPLE__) && defined(__MACH__))
#include <GLUT/glut.h>
#elif defined(WIN32) || defined(_WIN32) || defined(__TOS_WIN__) || defined(PREDEF_COMPILER_VISUALC)
#include <windows.h>
#include <GL/glut.h>
#elif defined(linux) || defined(__linux) || defined(__linux__)
#include <GL/glut.h>
#endif
#include <cmath>
#include <cstdio>
#include "materiais.h"
#define PI 3.14159265

#define UP 0
#define DOWN 1
#define LEFT 2
#define RIGHT 3
#define W 4
#define A 5
#define S 6
#define D 7

#define OFF 0
#define ON 1
#define GRAY1    0.0, 0.0, 0.05, 1.0
#define GRAY2    0.5, 0.8, 0.9, 1.0

void init();
void display();
void idle();
void keyboard(unsigned char, int, int);
void keyboardUp(unsigned char, int, int);
void specialKeyboard(int, int, int);
void specialKeyboardUp(int, int, int);
void checkMovement();
void drawScene();
void showUser();
void light();

GLfloat lookAngle = PI/2;
GLfloat flashLightAngleH = PI/2;
GLfloat flashLightAngleV = PI/2;
GLfloat userPos[3] = {0.0,0.0,2.0};
GLfloat userLook[3] = {0.0,0.0,userPos[2]-sin(lookAngle)};
GLint keys[8] = {0};
GLfloat globalLightColor[4] = {1.0, 1.0, 1.0, 1.0};
GLfloat flashLightPos[4] = {0.0, 0.0, 2.0, 1.0};
GLfloat flashLightTarget[3] = {0.0, 0.0, flashLightPos[2]-sin(lookAngle)};
GLfloat flashLightDir[3] = {0.0, 0.0, -2.0};
GLfloat roofLightPos[4] = {0.0, 10.0, 0.0, 1.0};
GLint daytime = ON;
GLint flashlight = OFF;
GLint rooflight = OFF;

int main(int argc, char *argv[])
{
    glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGB|GLUT_DEPTH);
	glutInitWindowSize(768,512);
	glutInitWindowPosition(100,100);
	glutCreateWindow("Visita guiada");

	init();

	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutKeyboardUpFunc(keyboardUp);
	glutSpecialFunc(specialKeyboard);
	glutSpecialUpFunc(specialKeyboardUp);
	glutIdleFunc(idle);

	glutMainLoop();
	return 1;
}

void init(void)
{
    glClearColor(0.0,0.0,0.0,1.0);
    glShadeModel(GL_SMOOTH);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHTING);
    glEnable(GL_NORMALIZE);
    //glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
}

void display(void)
{
    if (daytime == OFF)
    {
        glClearColor(GRAY1);
    }
	else
    {
        glClearColor(GRAY2);
    }
    checkMovement();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    light();

    glViewport(0, 0, 256, 256);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-5,5,-5,5,-5,5);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(0, 1, 0, 0, 0, 0, 0, 0, -1);
    drawScene();
    showUser();

    glViewport(256, 0, 512, 512);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45, 1, 1, 50);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(userPos[0], userPos[1], userPos[2], userLook[0], userLook[1], userLook[2], 0, 1, 0);
    drawScene();

    glutSwapBuffers();
}

void drawScene()
{
    glColor4f(0.0,0.0,0.0,1.0);
    glBegin(GL_LINES);
        glVertex3f(-100,0,0);
        glVertex3f(100,0,0);
    glEnd();
    glBegin(GL_LINES);
        glVertex3f(0,-100,0);
        glVertex3f(0,100,0);
    glEnd();
    glBegin(GL_LINES);
        glVertex3f(0,0,-100);
        glVertex3f(0,0,100);
    glEnd();

    glPushMatrix();
        //glColor4f(1,0,0,1.0);
        glMaterialfv(GL_FRONT, GL_AMBIENT, obsidianAmb);
        glMaterialfv(GL_FRONT, GL_DIFFUSE, obsidianDif);
        glMaterialfv(GL_FRONT, GL_SPECULAR, obsidianSpec);
        glMaterialf(GL_FRONT, GL_SHININESS, obsidianCoef);
        glutSolidTeapot(0.5);
    glPopMatrix();

    glPushMatrix();
        //glColor4f(1,0,0,1.0);
        glMaterialfv(GL_FRONT, GL_AMBIENT, goldAmb);
        glMaterialfv(GL_FRONT, GL_DIFFUSE, goldDif);
        glMaterialfv(GL_FRONT, GL_SPECULAR, goldSpec);
        glMaterialf(GL_FRONT, GL_SHININESS, goldCoef);
        glTranslatef(0.5,0.5,2);
        glutSolidCube(0.5);
    glPopMatrix();

    glPushMatrix();
        //glColor4f(0,0,1,1.0);
        glMaterialfv(GL_FRONT, GL_AMBIENT, rubyAmb);
        glMaterialfv(GL_FRONT, GL_DIFFUSE, rubyDif);
        glMaterialfv(GL_FRONT, GL_SPECULAR, rubySpec);
        glMaterialf(GL_FRONT, GL_SHININESS, rubyCoef);
        glTranslatef(-1,0,-2);
        glRotatef(90,1.0,0.0,0.0);
        glutSolidTorus(0.25,0.75,30,30);
    glPopMatrix();

    glPushMatrix();
        //glColor4f(0,1,0,1.0);
        glMaterialfv(GL_FRONT, GL_AMBIENT, whiteRubberAmb);
        glMaterialfv(GL_FRONT, GL_DIFFUSE, whiteRubberDif);
        glMaterialfv(GL_FRONT, GL_SPECULAR, whiteRubberSpec);
        glMaterialf(GL_FRONT, GL_SHININESS, whiteRubberCoef);
        glTranslatef(-2,0,0.5);
        glRotatef(45,1.0,1.0,0.0);
        glutSolidTorus(0.25,0.75,30,30);
    glPopMatrix();

    glPushMatrix();
        //glColor4f(0,1,0,1.0);
        glMaterialfv(GL_FRONT, GL_AMBIENT, silverAmb);
        glMaterialfv(GL_FRONT, GL_DIFFUSE, silverDif);
        glMaterialfv(GL_FRONT, GL_SPECULAR, silverSpec);
        glMaterialf(GL_FRONT, GL_SHININESS, silverCoef);
        glTranslatef(2,-0.5,-2);
        glutSolidSphere(0.5, 30, 30);
    glPopMatrix();

    glPushMatrix();
        glMaterialfv(GL_FRONT, GL_AMBIENT, chromeAmb);
        glMaterialfv(GL_FRONT, GL_DIFFUSE, chromeDif);
        glMaterialfv(GL_FRONT, GL_SPECULAR, chromeSpec);
        glMaterialf(GL_FRONT, GL_SHININESS, chromeCoef);
        glBegin(GL_QUADS);
            glNormal3f(0,0,1);
            glVertex3f(-1,-1,-5);
            glVertex3f(1,-1,-5);
            glVertex3f(1,1,-5);
            glVertex3f(-1,1,-5);
        glEnd();
    glPopMatrix();
}

void showUser()
{
    glPushMatrix();
        //glColor4f(0.0,0.0,0.0,1.0);
        glMaterialfv(GL_FRONT, GL_AMBIENT, blueRubberAmb);
        glMaterialfv(GL_FRONT, GL_DIFFUSE, blueRubberDif);
        glMaterialfv(GL_FRONT, GL_SPECULAR, blueRubberSpec);
        glMaterialf(GL_FRONT, GL_SHININESS, blueRubberCoef);
        glTranslatef(userPos[0],userPos[1],userPos[2]);
        glutSolidSphere(0.25, 20, 20);
    glPopMatrix();
    glBegin(GL_LINES);
        glVertex3f(userPos[0],userPos[1],userPos[2]);
        glVertex3f(userLook[0],userLook[1],userLook[2]);
    glEnd();
}

void idle(void)
{
    glutPostRedisplay();
}

void keyboard(unsigned char key, int x, int y)
{
    switch(key)
    {
    case 27:
        exit(0);
    case 'n':
    case 'N':
        daytime = 1-daytime;
        break;
    case 'f':
    case 'F':
        flashlight = 1-flashlight;
        break;
    case 't':
    case 'T':
        rooflight = 1-rooflight;
        break;
    case 'w':
    case 'W':
        keys[W] = 1;
        break;
    case 'a':
    case 'A':
        keys[A] = 1;
        break;
    case 's':
    case 'S':
        keys[S] = 1;
        break;
    case 'd':
    case 'D':
        keys[D] = 1;
        break;
    }
}

void keyboardUp(unsigned char key, int x, int y)
{
    switch(key)
    {
    case 'w':
    case 'W':
        keys[W] = 0;
        break;
    case 'a':
    case 'A':
        keys[A] = 0;
        break;
    case 's':
    case 'S':
        keys[S] = 0;
        break;
    case 'd':
    case 'D':
        keys[D] = 0;
        break;
    }
}

void specialKeyboard(int key, int x, int y)
{
    switch(key)
    {
    case GLUT_KEY_UP:
        keys[UP] = 1;
        break;
    case GLUT_KEY_DOWN:
        keys[DOWN] = 1;
        break;
    case GLUT_KEY_LEFT:
        keys[LEFT] = 1;
        break;
    case GLUT_KEY_RIGHT:
        keys[RIGHT] = 1;
        break;
    }
}

void specialKeyboardUp(int key, int x, int y)
{
    switch(key)
    {
    case GLUT_KEY_UP:
        keys[UP] = 0;
        break;
    case GLUT_KEY_DOWN:
        keys[DOWN] = 0;
        break;
    case GLUT_KEY_LEFT:
        keys[LEFT] = 0;
        break;
    case GLUT_KEY_RIGHT:
        keys[RIGHT] = 0;
        break;
    }
}

void checkMovement()
{
    if(keys[UP]==1)
    {
        userPos[0]+=0.025*(cos(lookAngle));
        userPos[2]+=0.025*(sin(-lookAngle));
        flashLightPos[0]+=0.025*(cos(flashLightAngleH));
        flashLightPos[2]+=0.025*(sin(-flashLightAngleH));
        userLook[0]=userPos[0]+cos(lookAngle);
        userLook[2]=userPos[2]-sin(lookAngle);
        flashLightTarget[0]=flashLightPos[0]+cos(flashLightAngleH);
        flashLightTarget[2]=flashLightPos[2]-sin(flashLightAngleH);
    }
    if(keys[DOWN]==1)
    {
        userPos[0]-=0.025*(cos(lookAngle));
        userPos[2]-=0.025*(sin(-lookAngle));
        flashLightPos[0]-=0.025*(cos(flashLightAngleH));
        flashLightPos[2]-=0.025*(sin(-flashLightAngleH));
        userLook[0]=userPos[0]+cos(lookAngle);
        userLook[2]=userPos[2]-sin(lookAngle);
        flashLightTarget[0]=flashLightPos[0]+cos(flashLightAngleH);
        flashLightTarget[2]=flashLightPos[2]-sin(flashLightAngleH);
    }
    if(keys[LEFT]==1)
    {
        lookAngle+=0.025;
        flashLightAngleH+=0.025;
        userLook[0]=userPos[0]+cos(lookAngle);
        userLook[2]=userPos[2]-sin(lookAngle);
        flashLightTarget[0]=flashLightPos[0]+cos(flashLightAngleH);
        flashLightTarget[2]=flashLightPos[2]-sin(flashLightAngleH);
    }
    if(keys[RIGHT]==1)
    {
        lookAngle-=0.025;
        flashLightAngleH-=0.025;
        userLook[0]=userPos[0]+cos(lookAngle);
        userLook[2]=userPos[2]-sin(lookAngle);
        flashLightTarget[0]=flashLightPos[0]+cos(flashLightAngleH);
        flashLightTarget[2]=flashLightPos[2]-sin(flashLightAngleH);
    }
    if(keys[W]==1)
    {
        flashLightAngleV-=0.025;
        flashLightTarget[1]=flashLightPos[1]+cos(flashLightAngleV);
    }
    if(keys[S]==1)
    {
        flashLightAngleV+=0.025;
        flashLightTarget[1]=flashLightPos[1]+cos(flashLightAngleV);
    }
    if(keys[A]==1)
    {
        flashLightAngleH+=0.025;
        flashLightTarget[0]=flashLightPos[0]+cos(flashLightAngleH);
        flashLightTarget[2]=flashLightPos[2]-sin(flashLightAngleH);
    }
    if(keys[D]==1)
    {
        flashLightAngleH-=0.025;
        flashLightTarget[0]=flashLightPos[0]+cos(flashLightAngleH);
        flashLightTarget[2]=flashLightPos[2]-sin(flashLightAngleH);
    }
}

void light()
{
    switch(daytime)
    {
    case ON:
        globalLightColor[0] = 5.0;
        globalLightColor[1] = 8.0;
        globalLightColor[2] = 9.0;
        glLightModelfv(GL_LIGHT_MODEL_AMBIENT, globalLightColor);
        break;
    case OFF:
        globalLightColor[0] = 0.05;
        globalLightColor[1] = 0.05;
        globalLightColor[2] = 0.05;
        glLightModelfv(GL_LIGHT_MODEL_AMBIENT, globalLightColor);
        break;
    }

    switch(rooflight)
    {
    case ON:
        glEnable(GL_LIGHT0);
        glLightfv(GL_LIGHT0, GL_POSITION, roofLightPos);
        break;
    case OFF:
        glDisable(GL_LIGHT0);
        break;
    }

    switch(flashlight)
    {
    case ON:
        glEnable(GL_LIGHT1);
        glLightfv(GL_LIGHT1, GL_DIFFUSE, chromeDif);
        glLightfv(GL_LIGHT1, GL_SPECULAR, chromeSpec);
        glLightfv(GL_LIGHT1, GL_POSITION, flashLightPos);
        flashLightDir[0] = flashLightTarget[0]-flashLightPos[0];
        flashLightDir[1] = flashLightTarget[1]-flashLightPos[1];
        flashLightDir[2] = flashLightTarget[2]-flashLightPos[2];
        glLightfv(GL_LIGHT1, GL_SPOT_DIRECTION, flashLightDir);
        glLightf(GL_LIGHT1, GL_SPOT_EXPONENT, 100.0);
        glLightf(GL_LIGHT1, GL_SPOT_CUTOFF, 45.0);
        break;
    case OFF:
        glDisable(GL_LIGHT1);
        break;
    }
    glutPostRedisplay();
}
