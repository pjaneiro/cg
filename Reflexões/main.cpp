#if defined(__MACOSX__) || (defined(__APPLE__) && defined(__MACH__))
#include <GLUT/glut.h>
#elif defined(WIN32) || defined(_WIN32) || defined(__TOS_WIN__) || defined(PREDEF_COMPILER_VISUALC)
#include <windows.h>
#include <GL/glut.h>
#elif defined(linux) || defined(__linux) || defined(__linux__)
#include <GL/glut.h>
#endif
#include <stdlib.h>
#include "RgbImage.h"

using namespace std;

const float LADO_CAPA = 14.0;
const float LADO_CHAO = 37.0;

GLuint textureId[3];
float angulo = 0;
float posx = 0;

void desenhaCapa()
{

	glDisable(GL_TEXTURE_2D);

	glPushMatrix();
    glTranslatef(posx,0,0);
	glRotatef(-angulo, 1.0, 1.0, 0.0);


	glBegin(GL_QUADS);

	//Cima
	glColor3f(1.0, 1.0f, 1.0);
	glNormal3f(0.0, 1.0, 0.0);
	glVertex3f(-LADO_CAPA / 2, LADO_CAPA / 2, -0.25);
	glVertex3f(-LADO_CAPA / 2, LADO_CAPA / 2, 0);
	glVertex3f(LADO_CAPA / 2, LADO_CAPA / 2, 0);
	glVertex3f(LADO_CAPA / 2, LADO_CAPA / 2, -0.25);

	//Baixo
	glNormal3f(0.0, -1.0, 0.0);
	glVertex3f(-LADO_CAPA / 2, -LADO_CAPA / 2, -0.25);
	glVertex3f(LADO_CAPA / 2, -LADO_CAPA / 2, -0.25);
	glVertex3f(LADO_CAPA / 2, -LADO_CAPA / 2, 0);
	glVertex3f(-LADO_CAPA / 2, -LADO_CAPA / 2,0);

	//Esquerda
	glNormal3f(-1.0, 0.0, 0.0);
	glVertex3f(-LADO_CAPA / 2, -LADO_CAPA / 2, -0.25);
	glVertex3f(-LADO_CAPA / 2, -LADO_CAPA / 2, 0);
	glVertex3f(-LADO_CAPA / 2, LADO_CAPA / 2, 0);
	glVertex3f(-LADO_CAPA / 2, LADO_CAPA / 2, -0.25);

	glEnd();




	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, textureId[0]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);

	//Frente
	glNormal3f(0.0, 0.0, 1.0);
	glTexCoord2f(0.0, 0.0);
	glVertex3f(-LADO_CAPA / 2, -LADO_CAPA / 2, 0);
	glTexCoord2f(1.0, 0.0);
	glVertex3f(LADO_CAPA / 2, -LADO_CAPA / 2, 0);
	glTexCoord2f(1.0, 1.0);
	glVertex3f(LADO_CAPA / 2, LADO_CAPA / 2, 0);
	glTexCoord2f(0.0, 1.0);
	glVertex3f(-LADO_CAPA / 2, LADO_CAPA / 2, 0);
	glEnd();


	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, textureId[1]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);
	//Trás
	glNormal3f(0.0, 0.0, -1.0);
	glTexCoord2f(1.0, 0.0);
	glVertex3f(-LADO_CAPA / 2, -LADO_CAPA / 2, -0.25);
	glTexCoord2f(1.0f, 1.0);
	glVertex3f(-LADO_CAPA / 2, LADO_CAPA / 2, -0.25);
	glTexCoord2f(0.0, 1.0);
	glVertex3f(LADO_CAPA / 2, LADO_CAPA / 2, -0.25);
	glTexCoord2f(0.0, 0.0);
	glVertex3f(LADO_CAPA / 2, -LADO_CAPA / 2, -0.25);

	glEnd();

	glPopMatrix();
}


void desenhaChao()
{

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, textureId[2]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glBegin(GL_QUADS);

	glNormal3f(0, 1, 0);
	glTexCoord2f(0, 0);
	glVertex3f(-LADO_CHAO / 2, 0, LADO_CHAO / 2);
	glTexCoord2f(1, 0);
	glVertex3f(LADO_CHAO / 2, 0, LADO_CHAO / 2);
    glTexCoord2f(1, 1);
	glVertex3f(LADO_CHAO / 2, 0, -LADO_CHAO / 2);
	glTexCoord2f(0, 1);
	glVertex3f(-LADO_CHAO / 2, 0, -LADO_CHAO / 2);

	glEnd();
}


void keyboard(unsigned char key, int x, int y)
{
	switch (key)
	{


		case 27: //Escape
			exit(0);
	}
}

void teclasNotAscii(int key, int x, int y)
{

    if(key == GLUT_KEY_LEFT)
        if (posx>-30)
            posx-=0.75;

	if(key == GLUT_KEY_RIGHT)
        if (posx<30)
            posx+=0.75;

    glutPostRedisplay();
}

void loadTexture()
{

	RgbImage imag;

	glGenTextures(1, &textureId[0]);
	glBindTexture(GL_TEXTURE_2D, textureId[0]);

	imag.LoadBmpFile("vu_n_fc.bmp");
	glTexImage2D(GL_TEXTURE_2D, 0, 3,
				 imag.GetNumCols(),
				 imag.GetNumRows(), 0, GL_RGB, GL_UNSIGNED_BYTE,
				 imag.ImageData());


	glGenTextures(1, &textureId[1]);
	glBindTexture(GL_TEXTURE_2D, textureId[1]);



	imag.LoadBmpFile("vu_n_bc.bmp");
	glTexImage2D(GL_TEXTURE_2D, 0, 3,
				 imag.GetNumCols(),
				 imag.GetNumRows(), 0, GL_RGB, GL_UNSIGNED_BYTE,
				 imag.ImageData());

	glGenTextures(1, &textureId[2]);
	glBindTexture(GL_TEXTURE_2D, textureId[2]);


	imag.LoadBmpFile("ilbym.bmp");
	glTexImage2D(GL_TEXTURE_2D, 0, 3,
				 imag.GetNumCols(),
				 imag.GetNumRows(), 0, GL_RGB, GL_UNSIGNED_BYTE,
				 imag.ImageData());



}

void init()
{

	GLfloat nevoeiroCor[] = {0.75, 0.75, 0.75, 1.0};

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_FOG);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_NORMALIZE);
	glEnable(GL_COLOR_MATERIAL);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);



	glFogfv(GL_FOG_COLOR, nevoeiroCor);
	glFogi(GL_FOG_MODE, GL_LINEAR);
	glFogf(GL_FOG_START, 20.0);
	glFogf(GL_FOG_END, 80.0);

	loadTexture();

}

void resizeWindow(int w, int h)
{
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(66.0, (float)w / (float)h, 1.0, 200.0);
}

void display()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.0, 0.0, -40.0);
	glRotatef(30, 1, 0, 0);

	GLfloat ambientLight[] = {0.2, 0.3, 0.3, 1.0};
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambientLight);

	GLfloat lightColor[] = {0.7, 0.7, 0.7, 1.0};
	GLfloat lightPos[] = {-2 * LADO_CAPA, LADO_CAPA, 4 * LADO_CAPA, 1.0};
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightColor);
	glLightfv(GL_LIGHT0, GL_POSITION, lightPos);

	glPushMatrix();
        glTranslatef(0, LADO_CAPA, 0);
        desenhaCapa();
	glPopMatrix();

	//ALUNOS: DESENHO REFLEXAO
	//REFLEXÃO
	//1. Activa o uso do stencil buffer
	glEnable(GL_STENCIL_TEST);
    //2. Nao escreve no color buffer
    glColorMask(0, 0, 0, 0);
	//3. Torna inactivo o teste de profundidade
	glDisable(GL_DEPTH_TEST);
	//4. Coloca a 1 todos os pixels no stencil buffer que representam o chão
	glStencilFunc(GL_ALWAYS, 1, 1);
    glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE);
	//5. Desenhar o chão
	glPushMatrix();
        desenhaChao();
    glPopMatrix();
    //6. Activa a escrita de cor
    glColorMask(1, 1, 1, 1);
	//7. Activa o teste de profundidade
    glEnable(GL_DEPTH_TEST);
	//8. O stencil test passa apenas quando o pixel tem o valor 1 no stencil buffer
	glStencilFunc(GL_EQUAL, 1, 1);
    //9. Stencil buffer read-only
    glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
	//10. Desenha o objecto com a reflexão onde stencil buffer é 1
	glPushMatrix();
        glScalef(1.0,-1.0,1.0);
        desenhaCapa();
	glPopMatrix();
	//11. Desactiva a utilização do stencil buffer
    glDisable(GL_STENCIL_TEST);

    //Blending
	glEnable(GL_BLEND);
	glPushMatrix();
        glColor4f(1, 1, 1, 0.7);
        desenhaChao();
	glPopMatrix();
	glDisable(GL_BLEND);

	//FIM REFLEXÃO
	glutSwapBuffers();
}

void update(int valor)
{
	angulo += 1.0;
	if (angulo > 360)
    {
		angulo -= 360;
	}

	glutPostRedisplay();
	glutTimerFunc(24, update, 0);
}

int main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH | GLUT_STENCIL);
	glutInitWindowSize(400, 400);

	glutCreateWindow("I'll be your mirror (a partir de \"Drawing Reflections - videotutorialsrock.com\"");
	init();

	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
    glutSpecialFunc(teclasNotAscii);
	glutReshapeFunc(resizeWindow);
	glutTimerFunc(24, update, 0);

	glutMainLoop();
	return 0;
}
