#if defined(__MACOSX__) || (defined(__APPLE__) && defined(__MACH__))
#include <GLUT/glut.h>
#elif defined(WIN32) || defined(_WIN32) || defined(__TOS_WIN__) || defined(PREDEF_COMPILER_VISUALC)
#include <windows.h>
#include <GL/glut.h>
#elif defined(linux) || defined(__linux) || defined(__linux__)
#include <GL/glut.h>
#endif
#include <cmath>
#include <cstdio>
#include "materiais.h"
#define PI 3.14159265

#define FRONT 0
#define BACK 1

void init();
void display();
void idle();
void keyboard(unsigned char, int, int);
void drawScene();

GLfloat userPos[3] = {5.0, 0.5, 5.0};
GLfloat userLook[3] = {0.0, 0.0, 0.0};
GLfloat obj0Pos[3] = {0.0, 0.0, 0.0};
GLfloat obj1Pos[3] = {0.0, 0.0, 0.0};
GLfloat obj2Pos[3] = {0.1, 2.0, 0.1};
GLfloat lightPos[4] = {5.0, 10.0, 5.0, 1.0};
GLfloat lightAmb[4] = {0.1, 0.1, 0.1, 1.0};
GLfloat lightDif[4] = {0.8, 0.8, 0.8, 1.0};
GLfloat lightSpe[4] = {1.0, 1.0, 1.0, 1.0};
GLfloat rotation = PI/2;
GLfloat alpha = 0.8;
GLint side = FRONT;

int main(int argc, char *argv[])
{
    glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGB|GLUT_DEPTH);
	glutInitWindowSize(512,512);
	glutInitWindowPosition(100,100);
	glutCreateWindow("Visibilidade e Blending");

	init();

	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutIdleFunc(idle);

	glutMainLoop();
	return 1;
}

void init(void)
{
    glClearColor(0.0,0.0,0.0,1.0);
    glShadeModel(GL_SMOOTH);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHTING);
    glEnable(GL_NORMALIZE);
    glEnable(GL_CULL_FACE);
    glEnable(GL_COLOR_MATERIAL);
    glEnable(GL_BLEND);
    glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

void display(void)
{
    glClearColor(1.0, 1.0, 1.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_LIGHT0);
    glLightfv(GL_LIGHT0, GL_POSITION, lightPos);
    glLightfv(GL_LIGHT0, GL_AMBIENT, lightAmb);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDif);
    glLightfv(GL_LIGHT0, GL_SPECULAR, lightSpe);

    glViewport(0, 0, 512, 512);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45, 1, 1, 50);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(userPos[0], userPos[1], userPos[2], userLook[0], userLook[1], userLook[2], 0, 1, 0);
    drawScene();

    glutSwapBuffers();
}

void drawScene()
{
    glColor4f(0.0,0.0,0.0,1.0);
    glBegin(GL_LINES);
        glVertex3f(0,0,0);
        glVertex3f(100,0,0);
    glEnd();
    glBegin(GL_LINES);
        glVertex3f(0,0,0);
        glVertex3f(0,100,0);
    glEnd();
    glBegin(GL_LINES);
        glVertex3f(0,0,0);
        glVertex3f(0,0,100);
    glEnd();

    if(sqrt(pow(obj0Pos[0]-userPos[0],2) + pow(obj0Pos[1]-userPos[1],2) + pow(obj0Pos[2]-userPos[2],2)) > sqrt(pow(obj1Pos[0]-userPos[0],2) + pow(obj1Pos[1]-userPos[1],2) + pow(obj1Pos[2]-userPos[2],2)))
    {
        glPushMatrix();
            glColor4f(1.0, 1.0, 0.0, alpha);
            if(side==FRONT)
            {
                glCullFace(GL_FRONT);
            }
            else
            {
                glCullFace(GL_BACK);
            }
            glTranslatef(obj0Pos[0],obj0Pos[1],obj0Pos[2]);
            glutSolidTeapot(0.5);
        glPopMatrix();

        glPushMatrix();
            glColor4f(0.9, 0.9, 0.9, alpha);
            if(side==FRONT)
            {
                glCullFace(GL_BACK);
            }
            else
            {
                glCullFace(GL_FRONT);
            }
            rotation+=PI/180;
            obj1Pos[0] = 1.5*cos(rotation);
            obj1Pos[2] = 1.5*-sin(rotation);
            glTranslatef(obj1Pos[0],obj1Pos[1],obj1Pos[2]);
            glRotatef(45, 1.0, 0.0, 0.0);
            glutSolidTorus(0.25, 0.5, 50, 50);
        glPopMatrix();
    }
    else
    {
        glPushMatrix();
            glColor4f(0.9, 0.9, 0.9, alpha);
            if(side==FRONT)
            {
                glCullFace(GL_BACK);
            }
            else
            {
                glCullFace(GL_FRONT);
            }
            rotation+=PI/180;
            obj1Pos[0] = 1.5*cos(rotation);
            obj1Pos[2] = 1.5*-sin(rotation);
            glTranslatef(obj1Pos[0],obj1Pos[1],obj1Pos[2]);
            glRotatef(45, 1.0, 0.0, 0.0);
            glutSolidTorus(0.25, 0.5, 50, 50);
        glPopMatrix();

        glPushMatrix();
            glColor4f(1.0, 1.0, 0.0, alpha);
            if(side==FRONT)
            {
                glCullFace(GL_FRONT);
            }
            else
            {
                glCullFace(GL_BACK);
            }
            glTranslatef(obj0Pos[0],obj0Pos[1],obj0Pos[2]);
            glutSolidTeapot(0.5);
        glPopMatrix();
    }


    glPushMatrix();
        glColor4f(0.0, 1.0, 1.0, 1.0);
        if(side==FRONT)
        {
            glCullFace(GL_FRONT);
        }
        else
        {
            glCullFace(GL_BACK);
        }
        glTranslatef(obj2Pos[0],obj2Pos[1],obj2Pos[2]);
        glRotatef(20, 1.0, 0.0, 1.0);
        glRotatef(20, 0.0, 1.0, 0.0);
        glutSolidTeapot(0.5);
    glPopMatrix();
}

void idle(void)
{
    glutPostRedisplay();
}

void keyboard(unsigned char key, int x, int y)
{
    switch(key)
    {
    case 27:
        exit(0);
    case 'q':
    case 'Q':
        lightAmb[0]+=0.1;
        lightAmb[1]+=0.1;
        lightAmb[2]+=0.1;
        break;
    case 'a':
    case 'A':
        lightAmb[0]-=0.1;
        lightAmb[1]-=0.1;
        lightAmb[2]-=0.1;
        break;
    case 'w':
    case 'W':
        lightDif[0]+=0.1;
        lightDif[1]+=0.1;
        lightDif[2]+=0.1;
        break;
    case 's':
    case 'S':
        lightDif[0]-=0.1;
        lightDif[1]-=0.1;
        lightDif[2]-=0.1;
        break;
    case 'e':
    case 'E':
        if(alpha<1.0)
            alpha+=0.05;
        break;
    case 'd':
    case 'D':
        if(alpha>0.0)
            alpha-=0.05;
        break;
    case 'c':
    case 'C':
        side=1-side;
        break;
    }
}
