#if defined(__MACOSX__) || (defined(__APPLE__) && defined(__MACH__))
#include <GLUT/glut.h>
#elif defined(WIN32) || defined(_WIN32) || defined(__TOS_WIN__) || defined(PREDEF_COMPILER_VISUALC)
#include <windows.h>
#include <GL/glut.h>
#elif defined(linux) || defined(__linux) || defined(__linux__)
#include <GL/glut.h>
#endif

#include <cmath>
#include <ctime>
#include <cstring>
#include <cstdio>

void init();
void display();
void keyboard(unsigned char, int, int);
void idle(void);
void desenhaQuadrado();

GLfloat x = 0.5, y = -0.5, alfa = 45;
GLfloat B = 3, A = 1;

int main(int argc, char* argv[])
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGB);
	glutInitWindowSize(512,512);
	glutInitWindowPosition(100,100);
	glutCreateWindow("Snake");

	init();

	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutIdleFunc(idle);

	glutMainLoop();
	return 1;
}

void init(void)
{
    glClearColor(0.0,0.0,0.0,1.0);
    glOrtho(-10,10,-10,10,-10,10);
    glShadeModel(GL_FLAT);
}

void display(void)
{
    glClear(GL_COLOR_BUFFER_BIT);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-10,10,-10,10,-10,10);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glColor4f(1.0,0.0,0.0,1.0);
    glTranslatef(x,y,0);
    glScalef(B,A,1);
    desenhaQuadrado();
    glLoadIdentity();
    glColor4f(0.0,1.0,0.0,1.0);
    glTranslatef(x+(B/2), y+(A/2), 0);
    glRotatef(alfa,0,0,1);
    glTranslatef(B/2, A/2, 0);
    glScalef(B,A,1);
    desenhaQuadrado();

    glutSwapBuffers();
}

void desenhaQuadrado(void)
{
    glBegin(GL_QUADS);
        glVertex3f(-0.5,-0.5,0);
        glVertex3f(-0.5,0.5,0);
        glVertex3f(0.5,0.5,0);
        glVertex3f(0.5,-0.5,0);
    glEnd();
}

void keyboard(unsigned char key, int x, int y)
{
    switch(key)
    {
    case 27:
        exit(0);
        break;
    }
}

void idle(void)
{
	glutPostRedisplay();
}
