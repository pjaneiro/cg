#if defined(__MACOSX__) || (defined(__APPLE__) && defined(__MACH__))
#include <GLUT/glut.h>
#elif defined(WIN32) || defined(_WIN32) || defined(__TOS_WIN__) || defined(PREDEF_COMPILER_VISUALC)
#include <windows.h>
#include <GL/glut.h>
#elif defined(linux) || defined(__linux) || defined(__linux__)
#include <GL/glut.h>
#endif

#include <cmath>
#include <ctime>
#include <cstring>
#include <cstdio>

#define LEFT_RIGHT_JUMP 0.03
#define ARM_JUMP 3
#define EDGE_JUMP 3
#define SQUARE_JUMP 0.01
#define NE 0
#define SE 1
#define SW 2
#define NW 3
#define MAXMOVES 3

void init();
void display();
void keyboard(unsigned char, int, int);
void specialKeyboard(int, int, int);
void idle(void);
void moveSquare(void);
void checkColision(void);
void displayScore(void);
void gameOver(void);

GLfloat baseX, armAngle, edgeAngle;
GLfloat squareX, squareY;
GLint squareMovement, movesLeft;
GLfloat posSquare[4][4], posBase[4][4], posEdge[4][4];
GLint tentativas, apanhados, embates;
GLfloat distSquareEdgeX, distSquareEdgeY, distSquareEdgeTotal;
GLfloat distSquareBaseX, distSquareBaseY, distSquareBaseTotal;
GLchar string[256];

int main(int argc, char* argv[])
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGB);
	glutInitWindowSize(512,512);
	glutInitWindowPosition(100,100);
	glutCreateWindow("Final Combat");

	init();

	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutSpecialFunc(specialKeyboard);
	glutIdleFunc(idle);

	glutMainLoop();
	return 1;
}

void init(void)
{
    glClearColor(0.0,0.0,0.0,1.0);
    gluOrtho2D(-1,1,-1,1);
    glShadeModel(GL_FLAT);
    srand(time(0));
    baseX = 0.0;
    armAngle = 0.0;
    edgeAngle = 0.0;
    squareX = -1.0;
    squareY = (-1.0)+(GLfloat)rand()/(GLfloat)(RAND_MAX/2.0);
    squareMovement = NE;
    movesLeft = MAXMOVES;
    tentativas = 1;
}

void display(void)
{
    glClear(GL_COLOR_BUFFER_BIT);

    glPushMatrix();
        glTranslatef(baseX,0.0,0.0);
        glPushMatrix();
            glScalef(4.0,1.0,1.0);
            glColor3f(1.0, 0.0, 0.0);
            glutSolidCube(0.1);
            glGetFloatv(GL_MODELVIEW_MATRIX, &posBase[0][0]);
        glPopMatrix();

        glRotatef(armAngle, 0.0, 0.0, 1.0);
        glTranslatef(0.0,0.2,0.0);
        glPushMatrix();
            glScalef(1.0, 4.0, 1.0);
            glColor3f(0.0,1.0,0.0);
            glutSolidCube(0.1);
        glPopMatrix();

        glPushMatrix();
            glTranslatef(0.0,0.2,0.0);
            glRotatef(edgeAngle, 0.0, 0.0, 1.0);
            glScalef(4.0,1.0,0.0);
            glColor3f(1.0,1.0,1.0);
            glutSolidCube(0.1);
            glGetFloatv(GL_MODELVIEW_MATRIX, &posEdge[0][0]);
        glPopMatrix();
    glPopMatrix();

    glPushMatrix();
            glTranslatef(squareX, squareY, 0.0);
            glColor3f(1.0, 1.0, 0.0);
            glutSolidCube(0.05);
            glGetFloatv(GL_MODELVIEW_MATRIX, &posSquare[0][0]);
    glPopMatrix();

    if(embates<=apanhados+3)
    {
        checkColision();
        moveSquare();
        displayScore();
    }
    else
    {
        moveSquare();
        displayScore();
        movesLeft=1;
        gameOver();
    }

    glutSwapBuffers();
}

void keyboard(unsigned char key, int x, int y)
{
    switch(key)
    {
    case 27:
        exit(0);
        break;
    case 'e':
        edgeAngle+=EDGE_JUMP;
        break;
    case 'q':
        edgeAngle-=ARM_JUMP;
        break;
    }
}

void specialKeyboard(int key, int x, int y)
{
    switch(key)
    {
    case GLUT_KEY_LEFT:
        if(baseX-0.2-LEFT_RIGHT_JUMP >= -1.0)
        {
                baseX-=LEFT_RIGHT_JUMP;
        }
        break;
    case GLUT_KEY_RIGHT:
        if(baseX+0.2+LEFT_RIGHT_JUMP <= 1.0)
        {
                baseX+=LEFT_RIGHT_JUMP;
        }
        break;
    case GLUT_KEY_DOWN:
        armAngle+=ARM_JUMP;
        break;
    case GLUT_KEY_UP:
        armAngle-=ARM_JUMP;
        break;
    }
}

void idle(void)
{
	glutPostRedisplay();
}

void moveSquare(void)
{
    if(movesLeft == 0)
    {
        squareX = -1.0;
        squareY = (-1.0)+(GLfloat)rand()/(GLfloat)(RAND_MAX/2.0);
        squareMovement = NE;
        movesLeft = MAXMOVES;
        tentativas++;
        return;
    }
    switch(squareMovement)
    {
    case NE:
        if(squareY >= 1.0 && squareX >= 1.0)
        {
            squareMovement = SW;
            break;
        }
        else if(squareY >= 1.0)
        {
            squareMovement = SE;
            break;
        }
        else if(squareX >= 1.0)
        {
            squareMovement = NW;
            break;
        }
        else
        {
            squareX += SQUARE_JUMP;
            squareY += SQUARE_JUMP;
            return;
        }
    case SE:
        if(squareY <= -1.0 && squareX >= 1.0)
        {
            squareMovement = NW;
            break;
        }
        else if(squareY <= -1.0)
        {
            squareMovement = NE;
            break;
        }
        else if(squareX >= 1.0)
        {
            squareMovement = SW;
            break;
        }
        else
        {
            squareX += SQUARE_JUMP;
            squareY -= SQUARE_JUMP;
            return;
        }
    case SW:
        if(squareY <= -1.0 && squareX <= -1.0)
        {
            squareMovement = NE;
            break;
        }
        else if(squareY <= -1.0)
        {
            squareMovement = NW;
            break;
        }
        else if(squareX <= -1.0)
        {
            squareMovement = SE;
            break;
        }
        else
        {
            squareX -= SQUARE_JUMP;
            squareY -= SQUARE_JUMP;
            return;
        }
    case NW:
        if(squareY >= 1.0 && squareX <= -1.0)
        {
            squareMovement = SE;
            break;
        }
        else if(squareY >= 1.0)
        {
            squareMovement = SW;
            break;
        }
        else if(squareX <= -1.0)
        {
            squareMovement = NE;
            break;
        }
        else
        {
            squareX -= SQUARE_JUMP;
            squareY += SQUARE_JUMP;
            return;
        }
    }
    movesLeft--;
}

void checkColision(void)
{
    distSquareEdgeX = posSquare[3][0]-posEdge[3][0];
    distSquareEdgeY = posSquare[3][1]-posEdge[3][1];
    distSquareEdgeTotal = sqrt(pow(distSquareEdgeX,2) + pow(distSquareEdgeY,2));
    if(distSquareEdgeTotal<=0.2)
    {
        apanhados++;
        movesLeft = 0;
        return;
    }
    distSquareBaseX = posSquare[3][0]-posBase[3][0];
    distSquareBaseY = posSquare[3][1]-posBase[3][1];
    distSquareBaseTotal = sqrt(pow(distSquareBaseX,2) + pow(distSquareBaseY,2));
    if(distSquareBaseTotal<=0.2)
    {
        embates++;
        movesLeft = 0;
        return;
    }
}

void displayScore(void)
{
    glColor3f(1.0, 1.0, 1.0);
    glPushMatrix();
        sprintf(string, "Apanhados: %d", apanhados);
        glRasterPos2f(-0.95,0.95);
        for(int i=0; i<strlen(string); i++)
        {
            glutBitmapCharacter(GLUT_BITMAP_9_BY_15,string[i]);
        }
    glPopMatrix();
    glPushMatrix();
        sprintf(string, "Embates: %d", embates);
        glRasterPos2f(-0.95,0.9);
        for(int i=0; i<strlen(string); i++)
        {
            glutBitmapCharacter(GLUT_BITMAP_9_BY_15,string[i]);
        }
    glPopMatrix();
    glPushMatrix();
        sprintf(string, "Tentativas: %d", tentativas);
        glRasterPos2f(-0.95,0.85);
        for(int i=0; i<strlen(string); i++)
        {
            glutBitmapCharacter(GLUT_BITMAP_9_BY_15,string[i]);
        }
	glPopMatrix();
}

void gameOver(void)
{
    glPushMatrix();
        sprintf(string, "GAME OVER");
        glRasterPos2f(-0.2,0.2);
        for(int i=0; i<strlen(string); i++)
        {
            glutBitmapCharacter(GLUT_BITMAP_9_BY_15,string[i]);
        }
    glPopMatrix();
}
