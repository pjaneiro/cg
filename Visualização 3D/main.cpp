#if defined(__MACOSX__) || (defined(__APPLE__) && defined(__MACH__))
#include <GLUT/glut.h>
#elif defined(WIN32) || defined(_WIN32) || defined(__TOS_WIN__) || defined(PREDEF_COMPILER_VISUALC)
#include <windows.h>
#include <GL/glut.h>
#elif defined(linux) || defined(__linux) || defined(__linux__)
#include <GL/glut.h>
#endif
#include <cmath>
#include <cstdio>
#define PI 3.14159265

#define UP 0
#define DOWN 1
#define LEFT 2
#define RIGHT 3

void init();
void display();
void idle();
void keyboard(unsigned char, int, int);
void specialKeyboard(int, int, int);
void specialKeyboardUp(int, int, int);
void checkMovement();
void drawScene();
void showUser();

GLfloat lookAngle = PI/2;
GLfloat userPos[3] = {0.0,0.0,2.0};
GLfloat userLook[3] = {0.0,0.0,userPos[2]-sin(lookAngle)};
GLint keys[4] = {0};

int main(int argc, char *argv[])
{
    glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGB|GLUT_DEPTH);
	glutInitWindowSize(768,512);
	glutInitWindowPosition(100,100);
	glutCreateWindow("Visita virtual");

	init();

	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutSpecialFunc(specialKeyboard);
	glutSpecialUpFunc(specialKeyboardUp);
	glutIdleFunc(idle);

	glutMainLoop();
	return 1;
}

void init(void)
{
    glClearColor(1.0,1.0,1.0,1.0);
    glShadeModel(GL_FLAT);
    glEnable(GL_DEPTH_TEST);
}

void display(void)
{
    checkMovement();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glViewport(0, 0, 256, 256);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-5,5,-5,5,-5,5);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(0, 1, 0, 0, 0, 0, 0, 0, -1);
    drawScene();
    showUser();

    glViewport(256, 0, 512, 512);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45, 1, 1, 10);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(userPos[0], userPos[1], userPos[2], userLook[0], userLook[1], userLook[2], 0, 1, 0);
    drawScene();

    glutSwapBuffers();
}

void drawScene()
{
    glColor4f(0.0,0.0,0.0,1.0);
    glBegin(GL_LINES);
        glVertex3f(-100,0,0);
        glVertex3f(100,0,0);
    glEnd();
    glBegin(GL_LINES);
        glVertex3f(0,-100,0);
        glVertex3f(0,100,0);
    glEnd();
    glBegin(GL_LINES);
        glVertex3f(0,0,-100);
        glVertex3f(0,0,100);
    glEnd();

    glPushMatrix();
        glColor4f(1,0.5,0,1.0);
        glutSolidTeapot(0.5);
    glPopMatrix();

    glPushMatrix();
        glColor4f(1,0,0,1.0);
        glTranslatef(0.5,0.5,2);
        glutSolidCube(0.5);
    glPopMatrix();

    glPushMatrix();
        glColor4f(0,0,1,1.0);
        glTranslatef(-1,0,-2);
        glRotatef(90,1.0,0.0,0.0);
        glutSolidTorus(0.25,0.75,20,20);
    glPopMatrix();

    glPushMatrix();
        glColor4f(0,1,0,1.0);
        glTranslatef(-2,0,0.5);
        glRotatef(45,1.0,1.0,0.0);
        glutSolidTorus(0.25,0.75,20,20);
    glPopMatrix();

    glPushMatrix();
        glColor4f(0,1,0,1.0);
        glTranslatef(2,-0.5,-2);
        glutSolidSphere(0.5, 20, 20);
    glPopMatrix();

}

void showUser()
{
    glPushMatrix();
        glColor4f(0.0,0.0,0.0,1.0);
        glTranslatef(userPos[0],userPos[1],userPos[2]);
        glutSolidSphere(0.25, 20, 20);
    glPopMatrix();
    glBegin(GL_LINES);
        glVertex3f(userPos[0],userPos[1],userPos[2]);
        glVertex3f(userLook[0],userLook[1],userLook[2]);
    glEnd();
}

void idle(void)
{
    glutPostRedisplay();
}

void keyboard(unsigned char key, int x, int y)
{
    switch(key)
    {
    case 27:
        exit(0);
    }
}

void specialKeyboard(int key, int x, int y)
{
    switch(key)
    {
    case GLUT_KEY_UP:
        keys[UP] = 1;
        break;
    case GLUT_KEY_DOWN:
        keys[DOWN] = 1;
        break;
    case GLUT_KEY_LEFT:
        keys[LEFT] = 1;
        break;
    case GLUT_KEY_RIGHT:
        keys[RIGHT] = 1;
        break;
    }
    /*switch(key)
    {
    case GLUT_KEY_DOWN:
        userPos[0]-=0.1*(cos(lookAngle));
        userPos[2]-=0.1*(sin(-lookAngle));
        userLook[0]=userPos[0]+cos(lookAngle);
        userLook[2]=userPos[2]-sin(lookAngle);
        break;
    case GLUT_KEY_UP:
        userPos[0]+=0.1*(cos(lookAngle));
        userPos[2]+=0.1*(sin(-lookAngle));
        userLook[0]=userPos[0]+cos(lookAngle);
        userLook[2]=userPos[2]-sin(lookAngle);
        break;
    case GLUT_KEY_LEFT:
        lookAngle+=0.1;
        userLook[0]=userPos[0]+cos(lookAngle);
        userLook[2]=userPos[2]-sin(lookAngle);
        break;
    case GLUT_KEY_RIGHT:
        lookAngle-=0.1;
        userLook[0]=userPos[0]+cos(lookAngle);
        userLook[2]=userPos[2]-sin(lookAngle);
        break;
    }*/
}

void specialKeyboardUp(int key, int x, int y)
{
    switch(key)
    {
    case GLUT_KEY_UP:
        keys[UP] = 0;
        break;
    case GLUT_KEY_DOWN:
        keys[DOWN] = 0;
        break;
    case GLUT_KEY_LEFT:
        keys[LEFT] = 0;
        break;
    case GLUT_KEY_RIGHT:
        keys[RIGHT] = 0;
        break;
    }
}

void checkMovement()
{
    if(keys[UP]==1)
    {
        userPos[0]+=0.025*(cos(lookAngle));
        userPos[2]+=0.025*(sin(-lookAngle));
        userLook[0]=userPos[0]+cos(lookAngle);
        userLook[2]=userPos[2]-sin(lookAngle);
    }
    if(keys[DOWN]==1)
    {
        userPos[0]-=0.025*(cos(lookAngle));
        userPos[2]-=0.025*(sin(-lookAngle));
        userLook[0]=userPos[0]+cos(lookAngle);
        userLook[2]=userPos[2]-sin(lookAngle);
    }
    if(keys[LEFT]==1)
    {
        lookAngle+=0.025;
        userLook[0]=userPos[0]+cos(lookAngle);
        userLook[2]=userPos[2]-sin(lookAngle);
    }
    if(keys[RIGHT]==1)
    {
        lookAngle-=0.025;
        userLook[0]=userPos[0]+cos(lookAngle);
        userLook[2]=userPos[2]-sin(lookAngle);
    }
}
