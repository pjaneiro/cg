#include <stdlib.h>
#include <GL/glut.h>
//#include "glut.h"

GLfloat r, g, b;

void init (void) {

	glClearColor(1.0, 1.0, 1.0, 1.0);
	gluOrtho2D(-100, 100, -100, 100);
	glShadeModel(GL_FLAT);
	r = 1; g = 0; b = 0;
}

void draw (void) {

	glClear(GL_COLOR_BUFFER_BIT);

	glColor3f(r, g, b);
	glBegin(GL_POLYGON);
		glVertex2f(0.25, 0.25);
		glVertex2f(0.75, 0.25);
		glVertex2f(0.75, 0.75);
		glVertex2f(0.25, 0.75);
	glEnd();

	glutSwapBuffers();
}

void keyboard (unsigned char key, int x, int y) {
	
	switch(key) {
		case 'r':
			r = 1.0; g = 0.0; b = 0.0;
			glutPostRedisplay();
			break;
		case 'g':
			r = 0.0; g = 1.0; b = 0.0;
			glutPostRedisplay();
			break;
		case 'b':
			r = 0.0; g = 0.0; b = 1.0;
			glutPostRedisplay();
			break;
		case 27:
			exit(0);
			break;
	}
}

int main (int argc, char** argv) {

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize(600, 500);
	glutInitWindowPosition(200, 100);
	glutCreateWindow("Quadrado");

	init();

	glutDisplayFunc(draw);
	glutKeyboardFunc(keyboard);

	glutMainLoop();

	return 1;
}