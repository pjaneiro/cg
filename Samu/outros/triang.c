#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <GL/glut.h>
//#include "glut.h"

#define PI 3.1415

GLfloat r, g, b;
int delay = 20;
int hour, minute, second;
struct tm * current_time;


void init (void) {

	glClearColor(0.0, 0.0, 0.0, 1.0);
	gluOrtho2D(-100, 100, -100, 100);
	glShadeModel(GL_FLAT);
	r = 1; g = 0; b = 0;
}

void timer (int value) {

	time_t tempo = time(0);
	current_time = localtime(&tempo);
	hour = current_time->tm_hour;
	minute = current_time->tm_min;
	second = current_time->tm_sec;
	glutPostRedisplay();
	glutTimerFunc(delay,timer,1);
}

void drawTriangle (void) {

	glClear(GL_COLOR_BUFFER_BIT);
	glColor3f(r,g,b);
	glBegin(GL_POLYGON);
		glVertex2f(0,0);
		glVertex2f(-10,-20);
		glVertex2f(10,-20);
	glEnd();
}

void draw (void) {

	glClear(GL_COLOR_BUFFER_BIT);

	glColor3f(r, g, b);
	GLfloat angulo = 90-360.0*second/60.0;

	glPushMatrix();
		glTranslatef(50*cos(PI*angulo/180.0), 50*sin(PI*angulo/180.0), 0);
		glRotatef(90+angulo,0,0,1);
		drawTriangle();
	glPopMatrix();

	glutSwapBuffers();
}

void keyboard (unsigned char key, int x, int y) {
	
	switch(key) {
		case 'r':
			r = 1.0; g = 0.0; b = 0.0;
			glutPostRedisplay();
			break;
		case 'g':
			r = 0.0; g = 1.0; b = 0.0;
			glutPostRedisplay();
			break;
		case 'b':
			r = 0.0; g = 0.0; b = 1.0;
			glutPostRedisplay();
			break;
		case 27:
			exit(0);
			break;
	}
}

int main (int argc, char** argv) {

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize(600, 500);
	glutInitWindowPosition(200, 100);
	glutCreateWindow("Triang");

	init();

	glutDisplayFunc(draw);
	glutKeyboardFunc(keyboard);
	glutTimerFunc(delay,timer,1);

	glutMainLoop();

	return 1;
}