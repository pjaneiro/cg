
#include "stdlib.h"
#include "math.h"
#include "stdio.h"
#include <GL/glut.h>


//---------------------------------------------------------------------------
//-------------------------------------------------------  Variaveis globais
//---------------------------------------------------------------------------

GLfloat wC=10.0, hC=10.0;		    //.. coordenadas reais

GLfloat wSup=4.0, hSup=1.0;			//.. dimensoes do suporte
GLfloat wLig=0.8, hLig=4.0;			//.. dimensoes da ligação
GLfloat wExt=1.0, hExt=3.0;			//.. dimensoes da extremidade

GLfloat xSup=0.0,  incSup=0.45;		//.. posicao horizontal do suporte / incremento para deslocamento
GLfloat agLig=0.0,  incLig=6.0;		//.. angulo  da Ligacao / incremento para rotacao em graus
GLfloat agExt=90.0, incExt=8.0;		//.. angulo  da extremidade / incremento para rotacao

GLfloat qx=-wC, qy=0;				//.. posicao do quadrado que cai
GLfloat tam=0.55;					//.. tamanho do quadrado
GLint   msec=10;					//.. definicao do timer (actualizacao)

char texto[30];						//.. texto a ser escrito no ecran
GLint  tentativas=0;					//.. numero de blocos gerados
GLint   apanhados=0;					//.. apanhdos pela extremidade
GLint     embates=0;					//.. embates na base



//---------------------------------------------------------------------------
//----------------------------------------------------  Inicializa parâmetros 
//---------------------------------------------------------------------------

void inicializa(void){
	glClearColor (0, 0, 0, 1);
	gluOrtho2D(-wC,wC,-hC,hC);  
	glShadeModel(GL_SMOOTH);
	srand(1);
}

float aleatorio(GLint minimo, GLint maximo){
	GLfloat y;
	y = rand()%1000;
	return (minimo+ 0.001*y*(maximo-minimo));
}

//---------------------------------------------------------------------------
//-----------------------------------------------------------------  Desenhar
//---------------------------------------------------------------------------
void desenhaQuadrado(GLfloat x, GLfloat y, GLfloat dx, GLfloat dy, 
					 GLfloat r, GLfloat g, GLfloat b            )
{
	glColor3f(r, g, b);
	glBegin(GL_QUADS);	                  
		glVertex2f(x   , y+dy ); 
		glVertex2f(x   , y    );
		glVertex2f(x+dx, y    );
		glVertex2f(x+dx, y+dy );               
	glEnd();
}

void desenhaTexto(char *string, GLfloat x, GLfloat y) 
{  
	glRasterPos2f(x,y); 
	while (*string)
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, *string++); 
}


void bracoArticulado(GLfloat xSup, GLfloat agLig, GLfloat agExt){
	int i,j;
	
	glPushMatrix();
		//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Suporte vermelho
		glColor3f(1,0,0);
	    glTranslatef (xSup, 0.0, 0.0);
		glPushMatrix();
			glScalef (wSup, hSup, 1.0);
			glutSolidCube(1.0);
		glPopMatrix();

		//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX A fazer pelos alunos
		//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Ligacao verde
		//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Extremidade Azul
		
		// -------------------------- uso da matriz Model_View
		// Px = ??  
		// Py = ??

		//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX A fazer pelos alunos		
	
	glPopMatrix();
}





//---------------------------------------------------------------------------
//......................................................................Timer
//---------------------------------------------------------------------------
void Timer(int value) 
{
	//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX A fazer pelos alunos
	//   Movimento do quadrado
	//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX A fazer pelos alunos
	
	glutPostRedisplay();
	glutTimerFunc(msec,Timer, 1);
}


//---------------------------------------------------------------------------
//....................................................................Eventos
//---------------------------------------------------------------------------

void teclasNotAscii(int key, int x, int y)
{
	if(key == GLUT_KEY_LEFT) {
		xSup= xSup- incSup;
		glutPostRedisplay();
	}
	if(key == GLUT_KEY_RIGHT) {
		xSup= xSup+ incSup;
		glutPostRedisplay();
	} 
	if(key == GLUT_KEY_DOWN) {
		agLig= (int) (agLig - incLig) % 360;
		glutPostRedisplay();	
	}
	if(key == GLUT_KEY_UP) {
		agLig= (int) (agLig + incLig) % 360;
		glutPostRedisplay();	
	}
}

void teclado (unsigned char key, int x, int y){
	
	switch (key) {
	
	// ---------  angulo de rotacao da Extremidade
	case 'q':
		agExt= (int) (agExt+ incExt) % 360;
		glutPostRedisplay();
	break;
	case 'a':
		agExt= (int) (agExt- incExt) % 360;
		glutPostRedisplay();
	break;
	
	case 27:
		exit(0);
		break;
	default:
		break;
	}
}



//---------------------------------------------------------------------------
//.............................................................. Desenha Cena
//---------------------------------------------------------------------------
void desenhaJanela(void){
	int i,j;

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~limpa
	glClear (GL_COLOR_BUFFER_BIT);

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~BraçoArticulado
	bracoArticulado(xSup, agLig, agExt);

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Quadrado a cair
	desenhaQuadrado(qx,qy,tam,tam,1,1,0);
	
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Grelha de pontos
	glColor3f(1,1,1);
	glBegin(GL_POINTS);	                  
		for (i=-wC;i<wC;i++)
			for (j=-hC;j<hC;j++)
				glVertex2f(i,j);		
	glEnd();

	
	//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX A fazer pelos alunos
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// Reggras do jogo :  Apanha / embate 
	//   - apanha - o quadrado bate na extremidade
	//   - embate - o quadrado bate na base
	//   - o jogo termina quando ambates>=apanha+3

	//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX A fazer pelos alunos


	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Actualizar tentativas e apanhados
	tentativas = aleatorio(-100,100);
	embates    = aleatorio(-100,100);
	apanhados  = aleatorio(-100,100);
	//sprintf_s(texto, "Tentativas = %d", tentativas);
	desenhaTexto(texto, -wC+1, hC-3);
	//sprintf_s(texto, "Embates   = %d", embates);
	desenhaTexto(texto, -wC+1, hC-2);
	//sprintf_s(texto, "Apanhados = %d", apanhados);
	desenhaTexto(texto, -wC+1, hC-1);
		
	
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Informacao
	glColor3f(0,0,0.97);
	char t[]="{jh,pm}@dei.uc.pt";
	desenhaTexto(t, -wC+0.2, -hC+0.1);
	
	
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Refresh
	glutSwapBuffers();
}



//---------------------------------------------------------------------------
//.............................................................. Main 
//---------------------------------------------------------------------------
int main(int argc, char** argv){
	glutInit(&argc, argv);
	glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize (500, 500); 
	glutInitWindowPosition (300, 100);
	glutCreateWindow ("Teclas: left, right, up, down, q, a ");
	
	inicializa ();
	
    glutDisplayFunc(desenhaJanela); 
	glutKeyboardFunc(teclado);
	glutSpecialFunc(teclasNotAscii); 
	glutTimerFunc(msec, Timer, 1);
	
	glutMainLoop();
	return 0;
}


