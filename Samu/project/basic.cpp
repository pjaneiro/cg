#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <unistd.h>
#if defined(__MACOSX__) || (defined(__APPLE__) && defined(__MACH__))
#include <GLUT/glut.h>
#elif defined(WIN32) || defined(_WIN32) || defined(__TOS_WIN__) || defined(PREDEF_COMPILER_VISUALC)
#include <windows.h>
#include <GL/glut.h>
#elif defined(linux) || defined(__linux) || defined(__linux__)
#include <GL/glut.h>
#endif

#include "RgbImage.cpp"

//--------------------------------- DEFINIR CORES E PI
#define AZUL 		0.0, 0.4, 0.8, 1.0
#define AZUL_CEU	0.0, 0.0, 0.2, 1.0
#define VERMELHO	1.0, 0.0, 0.0, 1.0
#define AMARELO 	1.0, 1.0, 0.0, 1.0
#define VERDE 		0.0, 1.0, 0.0, 1.0
#define LARANJA 	1.0, 0.5, 0.1, 1.0
#define BRANCO 		1.0, 1.0, 1.0, 1.0
#define PRETO 		0.0, 0.0, 0.0, 1.0
#define CINZENTO	0.9, 0.9, 0.9, 1.0
#define COR_ROBOT	0.2, 0.2, 0.2, 0.4
#define CIN_ESCURO	0.1, 0.1, 0.1, 1.0
#define PI 			3.14159
#define N_DETALHE	360
#define MAX_ENI		100
#define BIG_DIVI	20



//------------------------------------------------------------------ BUEDA CENAS
GLint 		wScreen = 1200, hScreen = 600;
GLint 		arenaSize = 20000;
GLint 		wallSize = 2000;
GLint 		camType = 0;
GLint 		camRotate = 1;
GLint 		delay = 10;
GLint 		teclas[6] = {0};
GLuint 		texture[5];
RgbImage	imag;
int 		pontuacao = 0;
int 		floorText[BIG_DIVI][BIG_DIVI];

GLfloat		fogColor[] = {0.1, 0.1, 0.1, 1.0};
int 		fogOnOff = 1;

GLfloat		ambientLightNight[] = {0.8f, 0.8f, 0.8f, 1.0f};
GLfloat		ambientLightDay[] = {3.0f, 3.0f, 3.0f, 1.0f};
int 		dayNight = 0;

GLfloat		lightPos[] = {0.0, 0.0, 0.0, 1.0};
GLfloat		lightCutOff = 60.0;
GLfloat		lightColor[] = {10.0,10.0,10.0,0.0};
GLfloat		lightDir[] = {0.0,0.0,0.0,0.0};

GLfloat		focusPos[] = {10000.0, 800.0, 10000.0, 1.0};
GLfloat		focusColor[] = {10.0,10.0,10.0,0.0};

GLint 		cubePosX = 10000, cubePosY = 0, cubePosZ = 10000;
GLint 		cubeMove = 0 ,cubeY = 0;
GLint 		cubeVel = 40;
GLint 		cubeSlowRate = 2;
GLint 		cubeRaio = 300;
GLfloat		cubeAng = 0.0;
GLint		cubeAngStep = 2;

GLint 		missilRange = 0;
GLint 		missilDelay = 0;
GLint 		missilAux = 0;
GLint 		missilPosX = 0;
GLint 		missilPosY = 0;
GLint 		missilPosZ = 0;
GLint 		missilFinalPosX = -500;
GLint 		missilFinalPosY = 0;
GLint 		missilFinalPosZ = -500;
GLint 		missilInicialPosX = 0;
GLint 		missilInicialPosY = 0;
GLint 		missilInicialPosZ = 0;
GLint 		missilRadius = 350;
GLfloat		missilEffect = 0;
GLint 		missilLaunch = 0;
GLint 		missilCount = 0;
GLint 		missilSteps = 60;
GLint 		missilDecal = 0;

GLint 		eniNum = 30;
GLint 		eniVel = 50;
GLint 		eniPos[100][4] = {{-1}};



//------------------------------------------------------------------ FOG
void fog(void){
	glFogfv(GL_FOG_COLOR, fogColor);
	glFogi(GL_FOG_MODE, GL_EXP2);
	glFogf(GL_FOG_DENSITY, 0.00022);
}



//------------------------------------------------------------------ LIGHTS
void lights(void){
	//------------ Ambiente
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambientLightNight);
	//------------ Tecto
	glLightfv(GL_LIGHT0, GL_POSITION, lightPos);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightColor);
	glLightfv(GL_LIGHT0, GL_SPOT_CUTOFF, &lightCutOff);
	glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, lightDir);
	//------------ Foco
	glLightfv(GL_LIGHT1, GL_POSITION, focusPos);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, focusColor);
}



//------------------------------------------------------------------ TEXTURES
void textures(void){
	//------------ CHAO
	glGenTextures(1, &texture[0]);
	glBindTexture(GL_TEXTURE_2D, texture[0]);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	imag.LoadBmpFile("chao.bmp");
	glTexImage2D(GL_TEXTURE_2D, 0, 3, imag.GetNumCols() ,imag.GetNumRows(), 0, GL_RGB, GL_UNSIGNED_BYTE, imag.ImageData());
	//------------ CHAO
	glGenTextures(1, &texture[1]);
	glBindTexture(GL_TEXTURE_2D, texture[1]);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	imag.LoadBmpFile("chao1.bmp");
	glTexImage2D(GL_TEXTURE_2D, 0, 3, imag.GetNumCols() ,imag.GetNumRows(), 0, GL_RGB, GL_UNSIGNED_BYTE, imag.ImageData());
	//------------ CHAO
	glGenTextures(1, &texture[2]);
	glBindTexture(GL_TEXTURE_2D, texture[2]);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	imag.LoadBmpFile("chao2.bmp");
	glTexImage2D(GL_TEXTURE_2D, 0, 3, imag.GetNumCols() ,imag.GetNumRows(), 0, GL_RGB, GL_UNSIGNED_BYTE, imag.ImageData());
	//------------ PAREDE
	glGenTextures(1, &texture[3]);
	glBindTexture(GL_TEXTURE_2D, texture[3]);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	imag.LoadBmpFile("wall.bmp");
	glTexImage2D(GL_TEXTURE_2D, 0, 3, imag.GetNumCols() ,imag.GetNumRows(), 0, GL_RGB, GL_UNSIGNED_BYTE, imag.ImageData());
	//------------ DECAL
	glGenTextures(1, &texture[4]);
	glBindTexture(GL_TEXTURE_2D, texture[4]);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	imag.LoadBmpFile("decal.bmp");
	glTexImage2D(GL_TEXTURE_2D, 0, 3, imag.GetNumCols() ,imag.GetNumRows(), 0, GL_RGB, GL_UNSIGNED_BYTE, imag.ImageData());
}



//------------------------------------------------------------------ INIT
void init(void){
	glClearColor(PRETO);
	glShadeModel(GL_SMOOTH);
	fog();
	glEnable(GL_FOG);
	textures();
	glEnable(GL_TEXTURE_2D);
	lights();
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glDisable(GL_LIGHT1);
	glEnable(GL_DEPTH_TEST);

	//------------ POSICAO RANDOM DOS INIMIGOS
	int i,j;
	srand (time(NULL));
	for(i=0;i<eniNum;i++){
		do{
			eniPos[i][0] = (rand() % (arenaSize - 200)) + 101;
			eniPos[i][2] = (rand() % (arenaSize - 200)) + 101;
			eniPos[i][1] = 0;
		}while(sqrt(pow(abs(eniPos[i][0]-cubePosX),2)+pow(abs(eniPos[i][2]-cubePosZ),2)) < 4000);
	}

	//------------ CHAO RANDOM
	int aux;
	for(i=0;i<BIG_DIVI;i++){
		for(j=0;j<BIG_DIVI;j++){
			aux = rand() % 100;
			if(aux < 4) floorText[i][j] = 2;
			else if(aux < 10) floorText[i][j] = 1;
			else floorText[i][j] = 0;
		}
	}
}



//------------------------------------------------------------------ TIMER
void timer(int value){

	//------------ LUZES
	lightPos[0] = cubePosX+(int)(sin(cubeAng*PI/180)*50);
	lightPos[1] = 100;
	lightPos[2] = cubePosZ+(int)(cos(cubeAng*PI/180)*50);
	lightDir[0] = (int)(sin(cubeAng*PI/180)*100);
	lightDir[1] = 100;
	lightDir[2] = (int)(cos(cubeAng*PI/180)*100);
	glLightfv(GL_LIGHT0, GL_POSITION, lightPos);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightColor);
	glLightfv(GL_LIGHT0, GL_SPOT_CUTOFF, &lightCutOff);
	glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, lightDir);

	glLightfv(GL_LIGHT1, GL_POSITION, focusPos);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, focusColor);

	//------------ TECLAS -> MOVIMENTO
	if(teclas[0] == 1) cubeMove = cubeVel;
	if(teclas[1] == 1) cubeMove = -cubeVel;
	if(teclas[2] == 1) cubeAng = (GLfloat) ( (int) ( cubeAng + cubeAngStep ) % 360 );
	if(teclas[3] == 1) cubeAng = (GLfloat) ( (int) ( cubeAng - cubeAngStep ) % 360 );
	if(teclas[4] == 1 && missilLaunch == 0) missilRange += 50;
	if(teclas[5] == 1 && cubePosY == 0) cubeY = 80;
	if(cubeAng < 0) cubeAng = 360 + cubeAng;
	
	//------------ MOVIMENTO
	int diffX = sin(cubeAng*PI/180)*cubeMove;
	if(cubePosX+cubeRaio+diffX < arenaSize && cubePosX-cubeRaio+diffX > 0) cubePosX += diffX;
	int diffZ = cos(cubeAng*PI/180)*cubeMove;
	if(cubePosZ+cubeRaio+diffZ < arenaSize && cubePosZ-cubeRaio+diffZ > 0) cubePosZ += diffZ;

	if(cubeMove < 0)
		cubeMove+=cubeSlowRate;
	if(cubeMove > 0)
		cubeMove-=cubeSlowRate;
	
	//------------ SALTO
	cubePosY += cubeY;
	
	if(cubePosY > 0){
		cubeY-=cubeSlowRate*2;
	}else{
		cubeY = 0;
	}

	//------------ ACTUALIZAR POSICAO DOS INIMIGOS
	int i;
	if(eniVel > 50){
		for(i=0;i<eniNum;i++){
			eniPos[i][0] += (int)(sin(((eniPos[i][3]+180)%360)*PI/180)*(eniVel/12.5));
			eniPos[i][2] += (int)(cos(((eniPos[i][3]+180)%360)*PI/180)*(eniVel/12.5));
			/*if(eniPos[i][0] < cubePosX) eniPos[i][0] += ((cubePosX-eniPos[i][0])/300)+eniVel/30.0;
			if(eniPos[i][0] > cubePosX) eniPos[i][0] -= ((eniPos[i][0]-cubePosX)/300)+eniVel/30.0;
			if(eniPos[i][2] < cubePosZ) eniPos[i][2] += ((cubePosZ-eniPos[i][2])/300)+eniVel/30.0;
			if(eniPos[i][2] > cubePosZ) eniPos[i][2] -= ((eniPos[i][2]-cubePosZ)/300)+eniVel/30.0;*/
		}
	}

	//------------ COLISOES
	for(i=0;i<eniNum;i++){
		if(sqrt(pow(abs(eniPos[i][0]-cubePosX),2)+pow(abs(eniPos[i][1]-cubePosY),2)+pow(abs(eniPos[i][2]-cubePosZ),2)) < 150){
			printf("\n#######   Pontuacao final ~> %3d   #######\n\n", pontuacao);
			exit(0);
		}
	}

	//------------ PARTE DO MISSIL
	if(missilRange > 0 && missilLaunch == 0){
		missilPosX = cubePosX+(int)(sin(cubeAng*PI/180)*missilRange);
		missilPosZ = cubePosZ+(int)(cos(cubeAng*PI/180)*missilRange);
		if(missilRange == missilAux){
			if(teclas[4] == 0){
				missilDecal = missilRange;
				missilFinalPosX = missilPosX;
				missilFinalPosY = missilPosY;
				missilFinalPosZ = missilPosZ;
				missilInicialPosX = cubePosX+(int)(sin((-90+cubeAng)*PI/180)*200);
				missilInicialPosY = cubePosY;
				missilInicialPosZ = cubePosZ+(int)(cos((-90+cubeAng)*PI/180)*200);
				missilPosX = missilInicialPosX;
				missilPosY = missilInicialPosY;
				missilPosZ = missilInicialPosZ;
				missilLaunch = 1;
				missilAux = 0;
			}
		}else{
			missilAux = missilRange;
		}
	}

	int aux = 0;
	if(missilLaunch == 1){
		if(missilFinalPosX > missilInicialPosX) missilPosX += ((float)(missilFinalPosX - missilInicialPosX) / missilSteps);
		if(missilFinalPosX < missilInicialPosX) missilPosX -= ((float)(missilInicialPosX - missilFinalPosX) / missilSteps);
		if(missilFinalPosZ > missilInicialPosZ) missilPosZ += ((float)(missilFinalPosZ - missilInicialPosZ) / missilSteps);
		if(missilFinalPosZ < missilInicialPosZ) missilPosZ -= ((float)(missilInicialPosZ - missilFinalPosZ) / missilSteps);
		if(missilDelay < missilSteps/2) missilPosY += (cubeVel/10)*((missilSteps/2)-missilDelay)/2;
		else missilPosY -= (cubeVel/10)*(missilDelay-(missilSteps/2))/2;
		missilDelay++;
		if(missilDelay == missilSteps){
			aux = 0;
			for(i=0;i<eniNum;i++){
				if(sqrt(pow(abs(eniPos[i][0]-missilFinalPosX),2)+pow(abs(eniPos[i][2]-missilFinalPosZ),2)) < missilEffect){
					aux++;
					do{
						eniPos[i][0] = (rand() % (arenaSize - 200)) + 101;
						eniPos[i][2] = (rand() % (arenaSize - 200)) + 101;
						eniPos[i][1] = 0;
					}while(sqrt(pow(abs(eniPos[i][0]-cubePosX),2)+pow(abs(eniPos[i][2]-cubePosZ),2)) < 8000);
				}
			}
			if(aux > 0){
				eniVel += aux;
				eniNum += aux;
				pontuacao += aux;
				if(eniNum > MAX_ENI) eniNum = MAX_ENI;
				printf("~ ~ ~ > %3d enimies are now alive! < ~ ~ ~\n", eniNum);
				cubeVel++;
			}
			if(eniNum-aux < 100){
				for(i=eniNum-aux;i<eniNum;i++){
					do{
						eniPos[i][0] = (rand() % (arenaSize - 200)) + 101;
						eniPos[i][2] = (rand() % (arenaSize - 200)) + 101;
						eniPos[i][1] = 0;
					}while(sqrt(pow(abs(eniPos[i][0]-cubePosX),2)+pow(abs(eniPos[i][2]-cubePosZ),2)) < 8000);
				}
			}
			missilLaunch = 0;
			missilDelay = 0;
			missilRange = 0;
		}
	}

	glutPostRedisplay();
	glutTimerFunc(delay,timer, 1);
}



//------------------------------------------------------------------ DRAW SCENE
void drawScene(int p1, int p2, int p3, int p4){
	int i,j;
	float textureColor = 0.3;

	//------------ EIXOS
	/*glColor4f(AZUL);
	glBegin(GL_LINES);
		glVertex3i( 100, 100, 100); 
		glVertex3i(1000, 100, 100); 
	glEnd();
	glColor4f(VERMELHO);
	glBegin(GL_LINES);
		glVertex3i(100,  100, 100); 
		glVertex3i(100, 1000, 100); 
	glEnd();
	glColor4f(VERDE);
	glBegin(GL_LINES);
		glVertex3i( 100, 100, 100); 
		glVertex3i( 100, 100,1000); 
	glEnd();*/

	glEnable(GL_COLOR_MATERIAL);
	glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);

	//------------ Chao
	for(i=0;i<BIG_DIVI;i++){
		for(j=0;j<BIG_DIVI;j++){
			glPushMatrix();
				glColor4f(textureColor,textureColor,textureColor,1.0);
				glEnable(GL_TEXTURE_2D);
				glBindTexture(GL_TEXTURE_2D,texture[floorText[i][j]]);
				glBegin(GL_POLYGON);
					glNormal3f(0.0,1.0,0.0);
					glTexCoord2d(0.0, 0.0); glVertex3f(i*(arenaSize/BIG_DIVI), 0, j*(arenaSize/BIG_DIVI));
					glTexCoord2d(0.0, 1.0); glVertex3f(i*(arenaSize/BIG_DIVI), 0, j*(arenaSize/BIG_DIVI)+(arenaSize/BIG_DIVI));
					glTexCoord2d(1.0, 1.0); glVertex3f(i*(arenaSize/BIG_DIVI)+(arenaSize/BIG_DIVI), 0, j*(arenaSize/BIG_DIVI)+(arenaSize/BIG_DIVI));
					glTexCoord2d(1.0, 0.0); glVertex3f(i*(arenaSize/BIG_DIVI)+(arenaSize/BIG_DIVI), 0, j*(arenaSize/BIG_DIVI));
				glEnd();
				glDisable(GL_TEXTURE_2D);
			glPopMatrix();
		}
	}

	//------------ Decal
	if(missilLaunch == 0){
		glPushMatrix();
		glColor4f(textureColor,textureColor,textureColor,1.0);
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D,texture[4]);
		glBegin(GL_POLYGON);
			glTexCoord2d(0.0, 0.0); glVertex3f(missilFinalPosX-(missilDecal/4), 5, missilFinalPosZ-(missilDecal/4));
			glTexCoord2d(1.0, 0.0); glVertex3f(missilFinalPosX+(missilDecal/4), 5, missilFinalPosZ-(missilDecal/4));
			glTexCoord2d(1.0, 1.0); glVertex3f(missilFinalPosX+(missilDecal/4), 5, missilFinalPosZ+(missilDecal/4));
			glTexCoord2d(0.0, 1.0); glVertex3f(missilFinalPosX-(missilDecal/4), 5, missilFinalPosZ+(missilDecal/4));
		glEnd();
		glDisable(GL_TEXTURE_2D);
		glPopMatrix();
	}

	//------------ Paredes
	if(p1 != 1){
		for(i=0;i<BIG_DIVI;i++){
			glPushMatrix();
			glColor4f(textureColor,textureColor,textureColor,1.0);
			glEnable(GL_TEXTURE_2D);
			glBindTexture(GL_TEXTURE_2D,texture[3]);
			glBegin(GL_POLYGON);
				glTexCoord2d(0.0, 0.0); glVertex3f(i*(arenaSize/BIG_DIVI), 0, 0);
				glTexCoord2d(1.0, 0.0); glVertex3f(i*(arenaSize/BIG_DIVI)+arenaSize/BIG_DIVI, 0, 0);
				glTexCoord2d(1.0, 1.0); glVertex3f(i*(arenaSize/BIG_DIVI)+arenaSize/BIG_DIVI, wallSize, 0);
				glTexCoord2d(0.0, 1.0); glVertex3f(i*(arenaSize/BIG_DIVI), wallSize, 0);
			glEnd();
			glDisable(GL_TEXTURE_2D);
			glPopMatrix();
		}
	}
	if(p2 != 1){
		for(i=0;i<BIG_DIVI;i++){
			glPushMatrix();
			glColor4f(textureColor,textureColor,textureColor,1.0);
			glEnable(GL_TEXTURE_2D);
			glBindTexture(GL_TEXTURE_2D,texture[3]);
			glBegin(GL_POLYGON);
				glTexCoord2d(0.0, 0.0); glVertex3f(arenaSize, 0, i*(arenaSize/BIG_DIVI));
				glTexCoord2d(1.0, 0.0); glVertex3f(arenaSize, 0, i*(arenaSize/BIG_DIVI)+arenaSize/BIG_DIVI);
				glTexCoord2d(1.0, 1.0); glVertex3f(arenaSize, wallSize, i*(arenaSize/BIG_DIVI)+arenaSize/BIG_DIVI);
				glTexCoord2d(0.0, 1.0); glVertex3f(arenaSize, wallSize, i*(arenaSize/BIG_DIVI));
			glEnd();
			glDisable(GL_TEXTURE_2D);
			glPopMatrix();
		}
	}
	if(p3 != 1){
		for(i=0;i<BIG_DIVI;i++){
			glPushMatrix();
			glColor4f(textureColor,textureColor,textureColor,1.0);
			glEnable(GL_TEXTURE_2D);
			glBindTexture(GL_TEXTURE_2D,texture[3]);
			glBegin(GL_POLYGON);
				glTexCoord2d(0.0, 0.0); glVertex3f(i*(arenaSize/BIG_DIVI), 0, arenaSize);
				glTexCoord2d(1.0, 0.0); glVertex3f(i*(arenaSize/BIG_DIVI)+arenaSize/BIG_DIVI, 0, arenaSize);
				glTexCoord2d(1.0, 1.0); glVertex3f(i*(arenaSize/BIG_DIVI)+arenaSize/BIG_DIVI, wallSize, arenaSize);
				glTexCoord2d(0.0, 1.0); glVertex3f(i*(arenaSize/BIG_DIVI), wallSize, arenaSize);
			glEnd();
			glDisable(GL_TEXTURE_2D);
			glPopMatrix();
		}
	}
	if(p4 != 1){
		for(i=0;i<BIG_DIVI;i++){
			glPushMatrix();
			glColor4f(textureColor,textureColor,textureColor,1.0);
			glEnable(GL_TEXTURE_2D);
			glBindTexture(GL_TEXTURE_2D,texture[3]);
			glBegin(GL_POLYGON);
				glTexCoord2d(0.0, 0.0); glVertex3f(0, 0, i*(arenaSize/BIG_DIVI));
				glTexCoord2d(1.0, 0.0); glVertex3f(0, 0, i*(arenaSize/BIG_DIVI)+arenaSize/BIG_DIVI);
				glTexCoord2d(1.0, 1.0); glVertex3f(0, wallSize, i*(arenaSize/BIG_DIVI)+arenaSize/BIG_DIVI);
				glTexCoord2d(0.0, 1.0); glVertex3f(0, wallSize, i*(arenaSize/BIG_DIVI));
			glEnd();
			glDisable(GL_TEXTURE_2D);
			glPopMatrix();
		}
	}

	//------------ Mira
	if(missilRange > 0 && missilLaunch == 0){
		glColor4f(BRANCO);
		glBegin(GL_LINE_LOOP);
		missilEffect = (sqrt(pow(abs(missilPosX-cubePosX),2)+pow(abs(missilPosZ-cubePosZ),2))/1500)*missilRadius;
		for(i=0;i<N_DETALHE;i++){
			glVertex3f(missilPosX+sin(i*PI/180)*missilEffect, 0, missilPosZ+cos(i*PI/180)*missilEffect);
		}
		glEnd();
	}

	//------------ Mira 2
	if(missilLaunch == 1){
		glColor4f(BRANCO);
		glBegin(GL_LINE_LOOP);
		for(i=0;i<N_DETALHE;i++){
			glVertex3f(missilFinalPosX+sin(i*PI/180)*missilEffect, 0, missilFinalPosZ+cos(i*PI/180)*missilEffect);
		}
		glEnd();
	}

	//------------ ROBOT
	glPushMatrix();
		glTranslatef(cubePosX, cubePosY, cubePosZ);
		glRotatef(cubeAng * camRotate, 0, 1, 0);
		glTranslatef(-150, 100, 0);
		glRotatef(90, 0, 1, 0);
		glColor4f(CIN_ESCURO);
		GLUquadricObj * quad = gluNewQuadric();
		gluCylinder(quad, 20.0, 20.0, 300.0, 10, 10);
		gluCylinder(quad, 100.0, 100.0, 20.0, 10, 10);
		glTranslatef(0, 0, 280);
		gluCylinder(quad, 100.0, 100.0, 20.0, 10, 10);
		glTranslatef(0, 0, -130);
		glColor4f(COR_ROBOT);glutSolidCube(cubeRaio/2);
		glTranslatef(0, 200, 0);
		glColor4f(COR_ROBOT);glutSolidCube(cubeRaio*0.7);

		glColor4f(CIN_ESCURO);
		glTranslatef(0, 0, -150);
		glRotatef(90,0,1,0);
		glRotatef(220,1,0,0);
		gluCylinder(quad, 40.0, 40.0, 300.0, 10, 10);
		glRotatef(-40,1,0,0);
		glTranslatef(-300, 0, -100);
		gluCylinder(quad, 40.0, 40.0, 300.0, 10, 10);
		glRotatef(-180,1,0,0);
		glRotatef(-90,0,1,0);
		glTranslatef(-100, 0, -150);
		
		glTranslatef(0, 160, 0);
		glColor4f(COR_ROBOT);glutSolidCube(cubeRaio*0.4);
	glPopMatrix();

	//------------ Enimigos
	float mult;
	for(i=0;i<eniNum;i++){
		if(eniPos[i][0] != -1 && eniPos[i][2] != -1){
			if(eniPos[i][0] >= cubePosX && eniPos[i][2] >= cubePosZ) eniPos[i][3] = (int)(atan(((eniPos[i][0]-cubePosX)*1.0)/((eniPos[i][2]-cubePosZ)*1.0))*180/PI);
			if(eniPos[i][0] >= cubePosX && eniPos[i][2] < cubePosZ) eniPos[i][3] = (int)(atan(((eniPos[i][0]-cubePosX)*1.0)/((eniPos[i][2]-cubePosZ)*1.0))*180/PI)+180;
			if(eniPos[i][0] < cubePosX && eniPos[i][2] >= cubePosZ) eniPos[i][3] = 360+(int)(atan(((cubePosX-eniPos[i][0])*1.0)/((cubePosZ-eniPos[i][2])*1.0))*180/PI);
			if(eniPos[i][0] < cubePosX && eniPos[i][2] < cubePosZ) eniPos[i][3] = (int)(atan(((cubePosX-eniPos[i][0])*1.0)/((cubePosZ-eniPos[i][2])*1.0))*180/PI)+180;
			
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			glPushMatrix();
				glTranslatef(eniPos[i][0], eniPos[i][1]+100, eniPos[i][2]);
				glRotatef((eniPos[i][3]+180)%360,0,1,0);
				glColor4f(COR_ROBOT);
				mult = 0.12;
				glutSolidCube(cubeRaio*mult);
				glTranslatef(0,80,0);
				mult = 0.22;
				glutSolidCube(cubeRaio*mult);
				glTranslatef(0,120,0);
				mult = 0.4;
				glutSolidCube(cubeRaio*mult);
				glTranslatef(80,40,50);
				glScalef(1,1,8);
				mult = 0.1;
				glutSolidCube(cubeRaio*mult);
				glTranslatef(-160,0,0);
				glScalef(1,1,1);
				mult = 0.1;
				glutSolidCube(cubeRaio*mult);
				glTranslatef(80,60,-10);
				glScalef(1,1,1/8.0);
				mult = 0.24;
				glutSolidCube(cubeRaio*mult);
			glPopMatrix();
			glDisable(GL_BLEND);
		}
	}

	//------------ MISSIL
	if(missilLaunch == 1){
		glPushMatrix();
			glColor4f(VERMELHO);
			glTranslatef(missilPosX, missilPosY, missilPosZ);
			glutSolidSphere( 60.0, 20.0, 20.0);
		glPopMatrix();
	}

	//------------ SOL
	if(dayNight == 1){
		glPushMatrix();
			glColor4f(AMARELO);
			glTranslatef(10000, 1800, 10000);
			glutSolidSphere( 60.0, 20.0, 20.0);
		glPopMatrix();
	}

	//------------ PONTOS PARA VER LUZ
	/*for(i=0;i<BIG_DIVI/4;i++){
		for(j=0;j<BIG_DIVI/4;j++){
			glPushMatrix();
				glColor4f(CIN_ESCURO);
				glTranslatef(i*arenaSize/(BIG_DIVI/4), 1000, j*arenaSize/(BIG_DIVI/4));
				glutSolidSphere( 60.0, 20.0, 20.0);
			glPopMatrix();
		}
	}*/
}



//------------------------------------------------------------------ DISPLAY
void display(void){
  	
	//------------ Apagar & Janela Visualizacao
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	glViewport (0,0,wScreen, hScreen);

	//------------ Projeccao
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(100.0, wScreen/hScreen, 0.5, 30000);
	
	//------------ Modelo + View
	int camX, camZ;
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	camX = cubePosX-(int)(sin(cubeAng*PI/180)*800);
	camZ = cubePosZ-(int)(cos(cubeAng*PI/180)*800);
	gluLookAt(camX, 1200+(camType*2100), camZ ,
		cubePosX+(int)(sin(cubeAng*PI/180)*500), 100, cubePosZ+(int)(cos(cubeAng*PI/180)*500) , 0,1,0);

	//------------ Objectos
	int p1 = 0, p2 = 0, p3 = 0, p4 = 0;
	if(camX < 0) p4 = 1;
	if(camX > arenaSize) p2 = 1;
	if(camZ < 0) p1 = 1;
	if(camZ > arenaSize) p3 = 1;
	drawScene(p1,p2,p3,p4);
	
	//------------ Actualizacao
	glutSwapBuffers();
}



//------------------------------------------------------------------ KEYBOARD
void keyboard_down(unsigned char key, int x, int y){
	switch (key){
	case 'w':
		teclas[0] = 1; break;
	case 's':
		teclas[1] = 1; break;
	case 'a':
		teclas[2] = 1; break;
	case 'd':
		teclas[3] = 1; break;
	case 'l':
		teclas[4] = 1; break;
	case ' ':
		teclas[5] = 1; break;

	//------------ MUDAR TIPO DE CAMERA
	case 'k':
		camType = (camType+1) % 2;
		break;
	case 'f':
		fogOnOff = !fogOnOff;
		if(fogOnOff == 1) glEnable(GL_FOG);
		else glDisable(GL_FOG);
		break;
	case 'g':
		dayNight = !dayNight;
		if(dayNight == 1){
			glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambientLightDay);
			glDisable(GL_LIGHT0);
			glEnable(GL_LIGHT1);
		}
		else{
			glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambientLightNight);
			glEnable(GL_LIGHT0);
			glDisable(GL_LIGHT1);
		}
		break;
	case 'o':
		if(camRotate == 0) camRotate = 1;
		else camRotate = 0;
		break;
	//------------ ESCAPE
	case 27:
		exit(0); break;
	}
}



//------------------------------------------------------------------ KEYBOARD UP
void keyboard_up(unsigned char key, int x, int y){
	switch (key){
	case 'w':
		teclas[0] = 0; break;
	case 's':
		teclas[1] = 0; break;
	case 'a':
		teclas[2] = 0; break;
	case 'd':
		teclas[3] = 0; break;
	case 'l':
		teclas[4] = 0; break;
	case ' ':
		teclas[5] = 0; break;
	}
}



//------------------------------------------------------------------ MAIN
int main(int argc, char** argv){

	glutInit(&argc, argv);											// lol dunno
	glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH );		// cenas dele
	glutInitWindowSize (wScreen, hScreen);							// tamanho da janela
	glutInitWindowPosition (0, 0);									// posicao da janela
	glutCreateWindow ("..:: S T K ::.. ~alfa-0.1~");				// texto da barra da janela
  
	init();															// inicializa cenas
	
	glutDisplayFunc(display);										// funcao de display
	glutKeyboardFunc(keyboard_down);								// tecla premida
	glutKeyboardUpFunc(keyboard_up);								// tecla solta
	glutTimerFunc(delay, timer, 1);									// funcao de timer :D

	glutMainLoop();

	return 0;
}


